import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

export default defineConfig({
  plugins: [vue()],
  server: {
    port: 8100
  },
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url)),
      "vue": "vue/dist/vue.esm-bundler.js"
    }
  },
  css: {
    preprocessorOptions: {
      scss: {
        additionalData: `
          @import "bulma/sass/utilities/_all";
        `
      }
    }
  }
})
