import axios from "axios"

import router from "@/router"

import handle401 from "./handle_401"
import { useToast } from 'vue-toastification'

const toast = useToast()

export class Instance {
	#api

	constructor(path = "") {
		this.#api = axios.create({
			baseURL: `${import.meta.env.VITE_API}/${path}`,
			withCredentials: true
		})
	}

	#req(method, params = { path: "", body: {} }) {
		const { path, body } = params
		return new Promise(( resolve, reject ) => {
			return this.#api[method](path || "", body)
				.then(res => resolve(res.data))
				.catch(err => {
					if (err.response?.status) {
						if (err.response?.data?.message) {
							toast.error(err.response.data.message)
						} else {
							const { status } = err.response
							
							switch (status) {
								case 400:
									toast.error("Переданны некорректные данные")
									break;
								case 401:
									handle401()
									localStorage.setItem("authorized", "")
									router.push({ name: "Login" })
									toast.error("Ошибка авторизации")
									break;
								case 403:
									router.push({ name: "Chats" })
									toast.error("Недостаточно прав доступа")
									break;
								case 404:
									toast.error("Данные не найдены")
									break;
								default:
									toast.error("Что-то не так с сервером")
									break;
							}
						}
					} else {
						if (err.message == "Network Error")
							toast.error("Нет связи с сервером")
						else
							toast.error("Что-то не так с кодом")
					}

					reject(err)
				})
		})
	}

	get(params) {
		return this.#req("get", params)
	}

	post(params) {
		return this.#req("post", params)
	}

	put(params) {
		return this.#req("put", params)
	}

	delete(params) {
		return this.#req("delete", params)
	}
}