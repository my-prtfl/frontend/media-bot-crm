import { useUserStore } from "@/stores/user"

export default () => {
	const us = useUserStore()
	us.user = null
}