import { library, dom } from "@fortawesome/fontawesome-svg-core"

import { faAngleLeft, faAngleRight, faArrowLeft, faArrowRight, faCheck, faCheckDouble, faCheckSquare, faCircleXmark, faEnvelope, faFaceSmile, faFile, faImage, faPaperPlane, faPen, faPlus, faRightFromBracket, faTrashCan, faXmarkSquare } from "@fortawesome/free-solid-svg-icons"

library.add(
	faRightFromBracket,
	faAngleRight,
	faAngleLeft,
	faArrowRight,
	faArrowLeft,
	faPaperPlane,
	faCheck,
	faCheckDouble,
	faTrashCan,
	faPlus,
	faEnvelope,
	faFile,
	faImage,
	faPen,
	faCircleXmark,
	faFaceSmile,
	faCheckSquare,
	faXmarkSquare
)

dom.watch()