import { createApp } from 'vue'
import App from './App.vue'

// SCSS
import "bulma/bulma.sass"
import "@/assets/scss/notifications.scss"

const app = createApp(App)

// Уведомления
import Toast, { POSITION } from "vue-toastification"
app.use(Toast, { position: POSITION.TOP_RIGHT })

// Icons
import "@/fontawesome/lib"

// Store
import { createPinia } from 'pinia'
app.use(createPinia())

// Router
import router from './router'
app.use(router)

import Card from "@/components/Card.vue"
app.component("b-card", Card)

import Loading from "@/components/Loading.vue"
app.component("b-loading", Loading)

app.mount('#app')
