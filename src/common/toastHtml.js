export default {
	template: "<p v-html=\"content\"></p>",
	props: {
		content: String
	}
}