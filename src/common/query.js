const esc = encodeURIComponent
export default params => {
	if (params && !!Object.keys(params).length) {
		return "?" + Object.keys(params).map(k => {
			return esc(k) 
				+ "=" 
				+ esc(
					!["string", "number"].includes(typeof params[k]) && !!params[k] ?
						Object.keys(params[k]).map(prop => params[k][prop]).join(",")
						: params[k] || ""
				)
		}).join("&")
	}

	return ""
}