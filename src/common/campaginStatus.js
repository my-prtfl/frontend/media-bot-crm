export default status => {
	switch(status) {
		case "created":
			return { text: "Создана", type: "warning" }
		case "pending":
			return { text: "Отправляется", type: "info" }
		case "stoped":
			return { text: "Остановлена", type: "danger" }
		default:
			return { text: "Отправлена", type: "success" }
	}
}