import { defineStore } from "pinia"
import { useUserStore } from "./user"
import { io } from "socket.io-client"

export const useSocket = defineStore("socket", {
	state: () => ({
		io: null
	}),
	getters: {
		emit: state => (...args) => state.io?.emit(...args),
		on: state => (...args) => state.io?.on(...args),
		uid: () => useUserStore().user?.id
	},
	actions: {
		startPooling() {
			this.io = io(
				import.meta.env.VITE_API,
				{
					withCredentials: true,
					// secure: import.meta.env.MODE == "production"
					secure: true
				}
			)
			this.io.emit("uid", this.uid)
		},
		stopPooling() {
			this.emit("close")
			this.io = null
		}
	}
})