import { defineStore } from "pinia"
import { Instance } from "@/api"

const api = new Instance("auth")
const uapi = new Instance("users")

export const useUserStore = defineStore("store", {
	state: () => ({
		user: null,
		users: []
	}),
	getters: {
		isAuthorized: state => !!state.user,
		isRoot: state => state.user?.root
	},
	actions: {
		async authByToken() {
      try {
        const user = await api.get()
        this.user = user
				
        localStorage.setItem("authorized", true)

        return true
      } catch (err) {
        console.error(err)
      }
    },
		async authByCredentials(body) {
			try {
				const user = await api.post({ body })
				this.user = user

				localStorage.setItem("authorized", true)
			} catch(err) {
				console.error(err)
			}
		},
		async logout() {
      try {
        await api.delete()
        localStorage.setItem("authorized", "")
        this.user = null
      } catch(err) {
        console.error(err)
      }
    },
		async getUsers() {
			try {
				const users = await uapi.get()
				this.users = users
			} catch(err) {
				console.error(err)
			}
		},
		createUser(body) {
			return uapi.post({ body })
		},
		deleteUser(id) {
			return uapi.delete({ path: `/${id}` })
		}
	}
})