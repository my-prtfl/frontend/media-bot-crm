import { defineStore } from "pinia"
import { Instance } from "@/api"
import query from "@/common/query"

const api = new Instance("campagins")

export const useCampaginStore = defineStore("campagin", {
	state: () => ({
		campagins: [],
		page: 1,
		last_page: 0,
		limit: 20
	}),
	getter: {},
	actions: {
		async getCampagins() {
			try {
				const params = {
					page: this.page,
					limit: this.limit
				}

				const { campagins, last_page } = await api.get({ path: query(params) })

				this.campagins = campagins
				this.last_page = last_page
			} catch(err) {
				console.error(err)
			}
		},
		createCampagin({ message, group }) {
			const formData = new FormData()

			for (let file of message.images) {
				formData.append("files", file)
			}

			for (let file of message.files) {
				formData.append("files", file)
			}

			formData.append("message", message.data)
			formData.append("group", group)

			return api.post({ body: formData })
		},
		getCampagin(id) {
			return api.get({ path: id })
		},
		startCampagin(id) {
			return api.post({ path: `${id}/start` })
		},
		stopCampagin(id) {
			return api.post({ path: `${id}/stop` })
		},
		removeCampagin(id) {
			return api.delete({ path: `/${id}` })
		}
	}
})