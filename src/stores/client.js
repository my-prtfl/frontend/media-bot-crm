import { defineStore } from "pinia"
import { Instance } from "@/api"
import query from "@/common/query"

const api = new Instance("clients")

export const useClientSrore = defineStore("client", {
	state: () => ({
		clients: [],
		page: 1,
		last_page: 0,
		limit: 20,
		filter: "",
		filter_type: "",
		groupName: ""
	}),
	getters: {},
	actions: {
		async getClients() {
			try {
				const params = {
					page: this.page,
					limit: this.limit,
					filter: this.filter,
					filter_type: this.filter_type,
					groupName: this.groupName
				}
				
				const { clients, last_page } = await api.get({ path: query(params) })

				this.clients = clients
				this.last_page = last_page
			} catch(err) {
				console.error(err)
			}
		},
		async getClient(path) {
			try {
				return await api.get({ path })
			} catch(err) {
				console.error(err)
			}
		},
		editClientGroup(groupId, clientId, action) {
			return api.put({ path: `/${clientId}`, body: { group_id: groupId, action } })
		},
		sendMessage(id, message) {
			const formData = new FormData()

			for (let file of message.images) {
				formData.append("files", file)
			}

			for (let file of message.files) {
				formData.append("files", file)
			}

			formData.append("content", message.data)

			return api.post({ path: `${id}/send_message`, body: formData })
		},
		retrySend(tgId, messageId) {
			return api.put({ path: `${tgId}/send_message`, body: { message_id: messageId } })
		}
	}
})