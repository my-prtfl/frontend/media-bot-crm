import { defineStore } from "pinia"
import { Instance } from "@/api"
import query from "@/common/query"

const api = new Instance("groups")

export const useGroupStore = defineStore("group", {
	state: () => ({
		groups: [],
		page: 1,
		last_page: 0,
		limit: 20,
		filter: null,
		filter_type: null
	}),
	getters: {
		notExistGroups: state => groups => {
			const ids = groups.filter(g => !!g).map(({ id }) => id)

			return state.groups.filter(g => !!g).filter(group => !ids.find(id => id == group.id))
		}
	},
	actions: {
		async getGroups() {
			try {
				const params = {
					page: this.page,
					limit: this.limit,
					filter: this.filter,
					filter_type: this.filter_type
				}

				const { groups, last_page } = await api.get({ path: query(params) })

				this.last_page = last_page
				this.groups = groups
			} catch(err) {
				console.error(err)
			}
		},
		createGroup(body) {
			return api.post({ body })
		},
		getGroup(id) {
			return api.get({ path: id })
		},
		editGroup(id, body) {
			return api.put({ path: id, body })
		},
		removeGroup(id) {
			return api.delete({ path: `/${id}` })
		}
	}
})