import { createRouter, createWebHistory } from 'vue-router'
import { useUserStore } from "@/stores/user"

import load from "./load"

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  linkExactActiveClass: "is-active",
  routes: [
    {
      path: "/",
      name: "Login",
      component: load("Login")
    },
    {
      path: "/chats",
      component: load("Wrap"),
      redirect: { name: "Chats" },
      children: [
        {
          path: "",
          name: "Chats",
          component: load("Chats")
        },
        {
          path: ":tg_id",
          name: "Chat",
          component: load("Chat")
        }
      ]
    },
    {
      path: "/groups",
      component: load("Wrap"),
      redirect: { name: "Groups" },
      children: [
        {
          path: "",
          name: "Groups",
          component: load("Groups")
        },
        {
          path: "add",
          name: "AddGroup",
          component: load("AddGroup")
        },
        {
          path: ":id",
          name: "Group",
          component: load("Group")
        }
      ]
    },
    {
      path: "/campagins",
      component: load("Wrap"),
      redirect: { name: "Campagins" },
      children: [
        {
          path: "",
          name: "Campagins",
          component: load("Campagins")
        },
        {
          path: "add",
          name: "AddCampagin",
          component: load("AddCampagin")
        },
        {
          path: ":id",
          name: "Campagin",
          component: load("Campagin")
        }
      ]
    },
    {
      path: "/managers",
      name: "Users",
      component: load("User")
    }
  ]
})

router.beforeResolve(to => {
  const us = useUserStore()

  if (!us.isAuthorized && to.name !== "Login")
    return { name: "Login" }
})

export default router
