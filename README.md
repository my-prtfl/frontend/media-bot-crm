# Media bot CRM

CRM for [Media bot](https://t.me/cryptopizza_bot)

Cannot work properly without [api](https://gitlab.com/my-prtfl/backend/media-bot)

## Stack
* Vite
* Vue 3
* Sass
* [Bulma](https://bulma.io)
* [Socket.io-client](https://socket.io)

## Functional:
* Chat with users 
*(User send messages and receive them in telegram bot, manager in CRM)*
* Ability to group users
* Ability to send newsletters
* Ability to create new Managers
